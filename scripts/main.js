/*************************************
      GLOBAL VARIABLES USED
**************************************/
var notesDiv,//element Divs
allNotesDiv,
singleNoteDiv,
savedNotesDiv,
savedNotesDocument = document.createDocumentFragment();//document Fragment for creating notes

/*********************************************************
      Create our Store object for saving our notes
**********************************************************/
store = {
  noteId: localStorage.Notes ? JSON.parse(localStorage.Notes).noteId:1,
  savedNotes: localStorage.Notes ? JSON.parse(localStorage.Notes).savedNotes:{},
  add: function(note){
    if (!note.id){
      note.id = store.noteId++;
      return !!(store.savedNotes[note.id] = note);
    }
  }
};

/******************************************
      Filling the AllNotes Div
      in a document Fragment
*******************************************/
function createAllNotes(){
  var notes, div, text, date, paragraph, head2, footer, infoText;//Variables used for creating elements
  
  if (!!localStorage.Notes){//If notes have been saved in local Storage
    if(!!allNotesDiv){
      allNotesDiv.innerHTML="";//Clear the div from previous info... if needed.
    };

    notes = JSON.parse(localStorage.Notes);//get our Notes

    for (var i = (notes.noteId-1); i >= 1; i--){ //for each note

      text = document.createTextNode(notes.savedNotes[i].text);//get text
      paragraph = document.createElement("p");
      date = document.createTextNode(notes.savedNotes[i].creationDate);//get date
      head = document.createElement("h2");
      div = document.createElement("div");
      infoText = document.createTextNode("LastUpdate: " + notes.savedNotes[i].creationDate
        + ", " +notes.savedNotes[i].creationTime
        );//get creation Date and time
      footer = document.createElement("p");

      //Append the corresponding info to the Elements
      footer.appendChild(infoText);
      head.appendChild(date);
      paragraph.appendChild(text);
      paragraph.id = i;
      div.appendChild(head);
      div.appendChild(paragraph);
      div.appendChild(footer);
      div.className = "all-mini-Notes";
      footer.className = "mini-notes-footer";
      savedNotesDocument.append(div);
    }
  }
};

/*****************************************
      Filling the Home page DAta
******************************************/
function createHomePage(){
    var miniDivs = document.getElementsByClassName("mini-Notes");
    var idNote = store.noteId - 1;

    for (i = 0; i < 2; i++){
      miniDivs[i].id = idNote;
      miniDivs[i].innerHTML = store.savedNotes[idNote]?(store.savedNotes[idNote].creationDate+", "+store.savedNotes[idNote].creationTime+"<br>"):"";
      miniDivs[i].innerHTML += store.savedNotes[idNote]?store.savedNotes[idNote--].preview:"...";
    }
};

/****************************************
      Constructor of our Notes Object
*****************************************/
function Notes (){
  this.creationDate = new Date().toDateString();
  this.creationTime = new Date().toTimeString().substr(0,7).replace(/(:)(\w(?![0-9]))/g,":0$2");
  this.text = getText();
  this.preview = this.text.substr(0,30)+"..."; 

  function getText(){
  	return document.getElementById("note-text").value;
  };

  store.add(this);
  localStorage.Notes = JSON.stringify(store);
  createHomePage();
};

/****************************************
      Home Div Button Event
*****************************************/
function saveNote(element){
  var createdNote;
  var el_id = element.target.id;

  if (element.target.value ==="saveNote"){//when clicked and with valid info... create a Note Object
    if(document.getElementById("note-text").value!==""){
      createdNote = new Notes();
    }
  }
};

/***********************************************************************
      Single Note page event manager... for updating or Deleting a Note 
************************************************************************/
function manageSingleNote(element){
  var id = singleNoteDiv.children[0].children[0].id;
  var noteToModify = JSON.parse(localStorage.Notes);

  if(element.target.id==="singleNote"){
    history.back();
  }

  if (element.target.value ==="Delete"){
    tempNoteData = store.savedNotes[id];
    store.savedNotes[id] = store.savedNotes[store.noteId-1];
    store.savedNotes[store.noteId-1] = tempNoteData;
    delete store.savedNotes[store.noteId-1];
    store.noteId--;
    localStorage.Notes = JSON.stringify(store);
    history.back();
  }

  if (element.target.value ==="Update"){
    if(noteToModify.savedNotes[id].text !== singleNoteDiv.children[0].children[0].textContent){
      store.savedNotes[id].text = singleNoteDiv.children[0].children[0].textContent;
      store.savedNotes[id].creationDate = new Date().toDateString();
      store.savedNotes[id].creationTime = new Date().toTimeString().substr(0,7).replace(/(:)(\w(?![0-9]))/g,":0$2");
      store.savedNotes[id].preview = store.savedNotes[id].text.substr(0,30)+"...";
      localStorage.Notes = JSON.stringify(store);
      history.back();
    }
  }
};

/*************************************************************
      "All Notes" page manager, for displaying single notes
**************************************************************/
function displaySingleNote(element){
  var noteToModify = JSON.parse(localStorage.Notes);
  var el_id = element.target.id;

  if (el_id.match(/[0-9]+/)){ //display single Note clicked and make it editable and visible
    singleNoteDiv.children[0].children[0].textContent="";
    singleNoteDiv.children[0].children[0].textContent = noteToModify.savedNotes[el_id].text;
    singleNoteDiv.children[0].children[0].contentEditable = "true";
    singleNoteDiv.children[0].children[0].id = el_id;
    allNotesDiv.style.visibility ="hidden";
    savedNotesDiv.style.visibility = "hidden";
    singleNoteDiv.style.visibility="visible";
    location.hash="#test";
  }
}

/**************************************************
      Manage Navigation through hash changes
***************************************************/
window.onhashchange=function(){
  if (location.hash === "#home"){
    createHomePage();
    allNotesDiv.style.visibility = "hidden";
    singleNoteDiv.style.visibility = "hidden";
    savedNotesDiv.style.visibility = "visible";
  }
  else if(location.hash ==="#allNotes"){
    createAllNotes();
    allNotesDiv.appendChild(savedNotesDocument);
    singleNoteDiv.style.visibility = "visible";
    savedNotesDiv.style.visibility = "hidden";
    allNotesDiv.style.visibility = "visible";
  }
}

/*************************************
      When page has loaded

      Get all the div elements used for displaying the info
      and adding events to main 4 divs of the single page
**************************************/
onload=function(){ 

  var today = new Date();//get todays Date

	notesDiv=document.getElementById("notes");
  notesDiv.addEventListener("click",saveNote);
  allNotesDiv = document.getElementById("allNotesContainer");
  allNotesDiv.addEventListener("click",displaySingleNote);
  singleNoteDiv=document.getElementById("singleNote");
  singleNoteDiv.addEventListener("click",manageSingleNote);
  savedNotesDiv= document.getElementById("savedNotes");
  savedNotesDiv.addEventListener("click", displaySingleNote);

/* Display de date in home Note element */
  document.getElementById("note-Date").innerHTML = today.toDateString() +"<br>";
/* Create home page*/
  createHomePage();

/* Set home as the default initial Page */
  location.hash="#home";
}
